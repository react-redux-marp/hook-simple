import React, {useState} from 'react';
import _ from 'lodash';
import ResourceList from './resource-list.js';

const App = (props) => {
    let [resource, setResource] = useState('posts');
    return (
        <div className="container-fluid">
            <div className="row my-3">
                <div className="col-12">
                    <button className="btn btn-success mr-4" onClick={() => setResource('posts')}>
                        Posts
                    </button>
                    <button className="btn btn-danger" onClick={() => setResource('todos')}>
                        Todos
                    </button>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <ResourceList resource={resource}/>
                </div>
            </div>
        </div>
    );
}

export default App;
