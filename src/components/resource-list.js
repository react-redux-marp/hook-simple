import React, {useState, useEffect} from 'react';
import axios from 'axios';
import _ from 'lodash';

const ajax = axios.create({
    baseURL: "https://jsonplaceholder.typicode.com/"
});

const useResources = (resource) => {

    const [resources, setResources] = useState([]);

    useEffect(() => {
        ajax
            .get(resource)
            .then((response) => {
                setResources([...response.data]);
            });
    },
    [resource]);

    return resources;

};

const Resource = (props) => {
    return (
        <div
        data-id={props.data.id}
        data-userid={props.data.userID}
        className={`px-2 py-1 mb-2 ${props.className}`}>
            {props.children}
        </div>
    );
};

const Todo = (props) => {
    return (
        <Resource
        data={props.data}
        className={`todo ${props.data.completed?'bg-success':'bg-danger'}`}>
        <h5>{props.data.title}</h5>
        </Resource>
    );
};

const Post = (props) => {
    return (
        <Resource
        data={props.data}
        className='border border-primary post'>
        <h5>{props.data.title}</h5>
        <div className="w-100"/>
        <p>{props.data.body}</p>
        </Resource>
    );
};

const ResourceList = ({resource}) => {
    let resources = useResources(resource);
    
    return (
        <div className="wrapper resources">
            {_.map(resources, (res) => res.completed===undefined?
                <Post key={res.id} data={res}/>:<Todo key={res.id} data={res}/>
            )}
        </div>
    );
};

export default ResourceList;